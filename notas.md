# "Consuming a RESTful Web Service"

## Notas

- Clase `RestTemplate` hace interactura con servicios RESTful sea muy fácil.
  - Puede incluso vincular datos a tipos dominio (modelos, POJO, ...).
- Anotación `@JsonIgnoreProperties(ignoreUnknow=true)` ignora al vincular JSON con modelo propiedades no existentes en modelo.
  - Si clave JSON y nombre atributo no coinciden usa `@JsonProperty("nombre-clave")` sobre atributo en modelo.
- Anotación `@Bean` es equivalente a configuración XML `<beans><bean name="..." class"com..."/></beans>`.
  - Se ejecuta método esta anotación y registra valor devuelto como una Bean en `BeanFactory`.
- Clase `Logger` crea clase mostrar logs por consola.
- Clase `RestTemplate` procesa datos JSON entrantes.
- Clase `CommandLineRunner` se ejcuta al iniciar la aplicación. En este ejemplo hace petición y muestra datos en consola.
- "Expresión Lambda" o función en línea: (parámetros) -> { Cuerpo } 

## Fuentes

- [Enlace](https://spring.io/guides/gs/consuming-rest/) a guía
- [Ejemplo @JsonProperty](https://stackoverflow.com/questions/12583638/when-is-the-jsonproperty-property-used-and-what-is-it-used-for)
- [Documentación](https://docs.spring.io/spring-javaconfig/docs/1.0.0.M4/reference/html/ch02s02.html) anotación @Bean